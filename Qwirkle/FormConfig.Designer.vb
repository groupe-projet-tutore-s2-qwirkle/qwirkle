﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_config
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.nmb_nbJoueurs = New System.Windows.Forms.NumericUpDown()
        Me.lbl_nbJoueurs = New System.Windows.Forms.Label()
        Me.lbl_titreQwirkle = New System.Windows.Forms.Label()
        Me.lbl_nomJoueurs = New System.Windows.Forms.Label()
        Me.lbl_nomJoueur1 = New System.Windows.Forms.Label()
        Me.lbl_nomJoueur2 = New System.Windows.Forms.Label()
        Me.lbl_nomJoueur3 = New System.Windows.Forms.Label()
        Me.lbl_nomJoueur4 = New System.Windows.Forms.Label()
        Me.txt_nomJoueur1 = New System.Windows.Forms.TextBox()
        Me.txt_nomJoueur2 = New System.Windows.Forms.TextBox()
        Me.txt_nomJoueur3 = New System.Windows.Forms.TextBox()
        Me.txt_nomJoueur4 = New System.Windows.Forms.TextBox()
        Me.btn_configCommencer = New System.Windows.Forms.Button()
        Me.btn_configQuitter = New System.Windows.Forms.Button()
        CType(Me.nmb_nbJoueurs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'nmb_nbJoueurs
        '
        Me.nmb_nbJoueurs.Location = New System.Drawing.Point(176, 82)
        Me.nmb_nbJoueurs.Maximum = New Decimal(New Integer() {4, 0, 0, 0})
        Me.nmb_nbJoueurs.Minimum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.nmb_nbJoueurs.Name = "nmb_nbJoueurs"
        Me.nmb_nbJoueurs.Size = New System.Drawing.Size(62, 22)
        Me.nmb_nbJoueurs.TabIndex = 0
        Me.nmb_nbJoueurs.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        'lbl_nbJoueurs
        '
        Me.lbl_nbJoueurs.AutoSize = True
        Me.lbl_nbJoueurs.Location = New System.Drawing.Point(33, 82)
        Me.lbl_nbJoueurs.Name = "lbl_nbJoueurs"
        Me.lbl_nbJoueurs.Size = New System.Drawing.Size(137, 17)
        Me.lbl_nbJoueurs.TabIndex = 1
        Me.lbl_nbJoueurs.Text = "Nombre de joueurs :"
        '
        'lbl_titreQwirkle
        '
        Me.lbl_titreQwirkle.AutoSize = True
        Me.lbl_titreQwirkle.Location = New System.Drawing.Point(135, 25)
        Me.lbl_titreQwirkle.Name = "lbl_titreQwirkle"
        Me.lbl_titreQwirkle.Size = New System.Drawing.Size(71, 17)
        Me.lbl_titreQwirkle.TabIndex = 3
        Me.lbl_titreQwirkle.Text = "QWIRKLE"
        '
        'lbl_nomJoueurs
        '
        Me.lbl_nomJoueurs.AutoSize = True
        Me.lbl_nomJoueurs.Location = New System.Drawing.Point(150, 158)
        Me.lbl_nomJoueurs.Name = "lbl_nomJoueurs"
        Me.lbl_nomJoueurs.Size = New System.Drawing.Size(123, 17)
        Me.lbl_nomJoueurs.TabIndex = 4
        Me.lbl_nomJoueurs.Text = "Nom des joueurs :"
        '
        'lbl_nomJoueur1
        '
        Me.lbl_nomJoueur1.AutoSize = True
        Me.lbl_nomJoueur1.Location = New System.Drawing.Point(36, 213)
        Me.lbl_nomJoueur1.Name = "lbl_nomJoueur1"
        Me.lbl_nomJoueur1.Size = New System.Drawing.Size(72, 17)
        Me.lbl_nomJoueur1.TabIndex = 5
        Me.lbl_nomJoueur1.Text = "Joueur 1 :"
        '
        'lbl_nomJoueur2
        '
        Me.lbl_nomJoueur2.AutoSize = True
        Me.lbl_nomJoueur2.Location = New System.Drawing.Point(36, 256)
        Me.lbl_nomJoueur2.Name = "lbl_nomJoueur2"
        Me.lbl_nomJoueur2.Size = New System.Drawing.Size(72, 17)
        Me.lbl_nomJoueur2.TabIndex = 6
        Me.lbl_nomJoueur2.Text = "Joueur 2 :"
        '
        'lbl_nomJoueur3
        '
        Me.lbl_nomJoueur3.AutoSize = True
        Me.lbl_nomJoueur3.Location = New System.Drawing.Point(36, 306)
        Me.lbl_nomJoueur3.Name = "lbl_nomJoueur3"
        Me.lbl_nomJoueur3.Size = New System.Drawing.Size(72, 17)
        Me.lbl_nomJoueur3.TabIndex = 7
        Me.lbl_nomJoueur3.Text = "Joueur 3 :"
        '
        'lbl_nomJoueur4
        '
        Me.lbl_nomJoueur4.AutoSize = True
        Me.lbl_nomJoueur4.Location = New System.Drawing.Point(36, 352)
        Me.lbl_nomJoueur4.Name = "lbl_nomJoueur4"
        Me.lbl_nomJoueur4.Size = New System.Drawing.Size(72, 17)
        Me.lbl_nomJoueur4.TabIndex = 8
        Me.lbl_nomJoueur4.Text = "Joueur 4 :"
        '
        'txt_nomJoueur1
        '
        Me.txt_nomJoueur1.Location = New System.Drawing.Point(138, 213)
        Me.txt_nomJoueur1.Margin = New System.Windows.Forms.Padding(3, 30, 3, 3)
        Me.txt_nomJoueur1.MaxLength = 18
        Me.txt_nomJoueur1.Name = "txt_nomJoueur1"
        Me.txt_nomJoueur1.Size = New System.Drawing.Size(162, 22)
        Me.txt_nomJoueur1.TabIndex = 9
        '
        'txt_nomJoueur2
        '
        Me.txt_nomJoueur2.Location = New System.Drawing.Point(138, 256)
        Me.txt_nomJoueur2.MaxLength = 18
        Me.txt_nomJoueur2.Name = "txt_nomJoueur2"
        Me.txt_nomJoueur2.Size = New System.Drawing.Size(162, 22)
        Me.txt_nomJoueur2.TabIndex = 10
        '
        'txt_nomJoueur3
        '
        Me.txt_nomJoueur3.Enabled = False
        Me.txt_nomJoueur3.Location = New System.Drawing.Point(138, 306)
        Me.txt_nomJoueur3.MaxLength = 18
        Me.txt_nomJoueur3.Name = "txt_nomJoueur3"
        Me.txt_nomJoueur3.Size = New System.Drawing.Size(162, 22)
        Me.txt_nomJoueur3.TabIndex = 11
        '
        'txt_nomJoueur4
        '
        Me.txt_nomJoueur4.Enabled = False
        Me.txt_nomJoueur4.Location = New System.Drawing.Point(138, 352)
        Me.txt_nomJoueur4.MaxLength = 18
        Me.txt_nomJoueur4.Name = "txt_nomJoueur4"
        Me.txt_nomJoueur4.Size = New System.Drawing.Size(162, 22)
        Me.txt_nomJoueur4.TabIndex = 12
        '
        'btn_configCommencer
        '
        Me.btn_configCommencer.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_configCommencer.Location = New System.Drawing.Point(39, 399)
        Me.btn_configCommencer.Name = "btn_configCommencer"
        Me.btn_configCommencer.Size = New System.Drawing.Size(104, 39)
        Me.btn_configCommencer.TabIndex = 13
        Me.btn_configCommencer.Text = "Commencer"
        Me.btn_configCommencer.UseVisualStyleBackColor = True
        '
        'btn_configQuitter
        '
        Me.btn_configQuitter.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_configQuitter.Location = New System.Drawing.Point(198, 399)
        Me.btn_configQuitter.Name = "btn_configQuitter"
        Me.btn_configQuitter.Size = New System.Drawing.Size(75, 39)
        Me.btn_configQuitter.TabIndex = 14
        Me.btn_configQuitter.Text = "Quitter"
        Me.btn_configQuitter.UseVisualStyleBackColor = True
        '
        'frm_config
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(342, 453)
        Me.ControlBox = False
        Me.Controls.Add(Me.btn_configQuitter)
        Me.Controls.Add(Me.btn_configCommencer)
        Me.Controls.Add(Me.txt_nomJoueur4)
        Me.Controls.Add(Me.txt_nomJoueur3)
        Me.Controls.Add(Me.txt_nomJoueur2)
        Me.Controls.Add(Me.txt_nomJoueur1)
        Me.Controls.Add(Me.lbl_nomJoueur4)
        Me.Controls.Add(Me.lbl_nomJoueur3)
        Me.Controls.Add(Me.lbl_nomJoueur2)
        Me.Controls.Add(Me.lbl_nomJoueur1)
        Me.Controls.Add(Me.lbl_nomJoueurs)
        Me.Controls.Add(Me.lbl_titreQwirkle)
        Me.Controls.Add(Me.lbl_nbJoueurs)
        Me.Controls.Add(Me.nmb_nbJoueurs)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(360, 500)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(360, 500)
        Me.Name = "frm_config"
        Me.Text = "Configuration de la partie"
        CType(Me.nmb_nbJoueurs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents nmb_nbJoueurs As NumericUpDown
    Friend WithEvents lbl_nbJoueurs As Label
    Friend WithEvents lbl_titreQwirkle As Label
    Friend WithEvents lbl_nomJoueurs As Label
    Friend WithEvents lbl_nomJoueur1 As Label
    Friend WithEvents lbl_nomJoueur2 As Label
    Friend WithEvents lbl_nomJoueur3 As Label
    Friend WithEvents lbl_nomJoueur4 As Label
    Friend WithEvents txt_nomJoueur1 As TextBox
    Friend WithEvents txt_nomJoueur2 As TextBox
    Friend WithEvents txt_nomJoueur3 As TextBox
    Friend WithEvents txt_nomJoueur4 As TextBox
    Friend WithEvents btn_configCommencer As Button
    Friend WithEvents btn_configQuitter As Button
End Class
