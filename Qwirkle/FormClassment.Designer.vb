﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_ClassementFin
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_recommencerPartie = New System.Windows.Forms.Button()
        Me.btn_classementFinQuitter = New System.Windows.Forms.Button()
        Me.lbl_classement4 = New System.Windows.Forms.Label()
        Me.lbl_classement3 = New System.Windows.Forms.Label()
        Me.lbl_classement2 = New System.Windows.Forms.Label()
        Me.lbl_classement1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btn_recommencerPartie
        '
        Me.btn_recommencerPartie.Location = New System.Drawing.Point(12, 155)
        Me.btn_recommencerPartie.Name = "btn_recommencerPartie"
        Me.btn_recommencerPartie.Size = New System.Drawing.Size(189, 36)
        Me.btn_recommencerPartie.TabIndex = 0
        Me.btn_recommencerPartie.Text = "Recommencer une partie"
        Me.btn_recommencerPartie.UseVisualStyleBackColor = True
        '
        'btn_classementFinQuitter
        '
        Me.btn_classementFinQuitter.Location = New System.Drawing.Point(263, 155)
        Me.btn_classementFinQuitter.Name = "btn_classementFinQuitter"
        Me.btn_classementFinQuitter.Size = New System.Drawing.Size(110, 36)
        Me.btn_classementFinQuitter.TabIndex = 1
        Me.btn_classementFinQuitter.Text = "Quitter"
        Me.btn_classementFinQuitter.UseVisualStyleBackColor = True
        '
        'lbl_classement4
        '
        Me.lbl_classement4.AutoSize = True
        Me.lbl_classement4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lbl_classement4.Location = New System.Drawing.Point(159, 118)
        Me.lbl_classement4.Name = "lbl_classement4"
        Me.lbl_classement4.Size = New System.Drawing.Size(77, 25)
        Me.lbl_classement4.TabIndex = 5
        Me.lbl_classement4.Text = "4ème  :"
        '
        'lbl_classement3
        '
        Me.lbl_classement3.AutoSize = True
        Me.lbl_classement3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_classement3.Location = New System.Drawing.Point(159, 84)
        Me.lbl_classement3.Name = "lbl_classement3"
        Me.lbl_classement3.Size = New System.Drawing.Size(77, 25)
        Me.lbl_classement3.TabIndex = 4
        Me.lbl_classement3.Text = "3ème  :"
        '
        'lbl_classement2
        '
        Me.lbl_classement2.AutoSize = True
        Me.lbl_classement2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_classement2.Location = New System.Drawing.Point(159, 48)
        Me.lbl_classement2.Name = "lbl_classement2"
        Me.lbl_classement2.Size = New System.Drawing.Size(77, 25)
        Me.lbl_classement2.TabIndex = 3
        Me.lbl_classement2.Text = "2ème  :"
        '
        'lbl_classement1
        '
        Me.lbl_classement1.AutoSize = True
        Me.lbl_classement1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lbl_classement1.Location = New System.Drawing.Point(166, 9)
        Me.lbl_classement1.Name = "lbl_classement1"
        Me.lbl_classement1.Size = New System.Drawing.Size(56, 25)
        Me.lbl_classement1.TabIndex = 2
        Me.lbl_classement1.Text = "1er  :"
        '
        'frm_ClassementFin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(407, 203)
        Me.ControlBox = False
        Me.Controls.Add(Me.lbl_classement4)
        Me.Controls.Add(Me.lbl_classement3)
        Me.Controls.Add(Me.lbl_classement2)
        Me.Controls.Add(Me.lbl_classement1)
        Me.Controls.Add(Me.btn_classementFinQuitter)
        Me.Controls.Add(Me.btn_recommencerPartie)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(425, 250)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(425, 250)
        Me.Name = "frm_ClassementFin"
        Me.Text = "Classement"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_recommencerPartie As Button
    Friend WithEvents btn_classementFinQuitter As Button
    Friend WithEvents lbl_classement4 As Label
    Friend WithEvents lbl_classement3 As Label
    Friend WithEvents lbl_classement2 As Label
    Friend WithEvents lbl_classement1 As Label
End Class
