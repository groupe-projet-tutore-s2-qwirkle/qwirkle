﻿Imports QwirkleLib


Public Class frm_Qwirkle

    'Image des Tuile
    Dim imageTuileBlanche As Image = Image.FromFile(".\imagestuiles\TuileBlanche.jpg")

    Function ImageTuile(couleur As String, symbole As String) As Image
        Dim path As String
        If (couleur Like "defaut" Or symbole Like "defaut") Then
            Return imageTuileBlanche
        Else
            symbole = StrConv(symbole, VbStrConv.ProperCase)
            couleur = StrConv(couleur, VbStrConv.ProperCase)
            path = ".\imagestuiles\" & symbole & couleur & ".jpg"
            Dim imageTuileRetourne As Image = Image.FromFile(path)
            Return imageTuileRetourne
        End If
    End Function



    'Setup de la fenetre/du jeu
    Private Sub frm_Qwirkle_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'Calcul largeur/hauteur
        Dim modulolargeur As Integer
        Dim modulohauteur As Integer
        modulolargeur = (Me.ClientSize.Width * 0.7) Mod 30
        modulohauteur = (Me.ClientSize.Width * 0.66) Mod 30

        'Taille des panels
        pnl_grilledejeu.Location = New Point(0, 0)
        pnl_grilledejeu.Size = New Size(Me.ClientSize.Width * 0.7 - modulolargeur, Me.ClientSize.Height * 0.8 - modulohauteur)
        pnl_affichagescore.Location = New Point(pnl_grilledejeu.Width, 0)
        pnl_affichagescore.Size = New Size(Me.ClientSize.Width - pnl_grilledejeu.Width, pnl_grilledejeu.Height)
        pnl_main.Location = New Point(0, pnl_grilledejeu.Height)
        pnl_main.Size = New Size(pnl_grilledejeu.Width, Me.ClientSize.Height - pnl_grilledejeu.Height)
        pnl_action.Location = New Point(pnl_grilledejeu.Width, pnl_grilledejeu.Height)
        pnl_action.Size = New Size(Me.ClientSize.Width - pnl_grilledejeu.Width, Me.ClientSize.Height - pnl_grilledejeu.Height)

        'Boutons panel actions
        btn_terminertour.Location = New Point(pnl_action.Width * 0.0625, pnl_action.Height * 0.0625)
        btn_terminertour.Size = New Size(pnl_action.Width * 0.875, pnl_action.Height * 0.25)
        btn_reinitialisation.Location = New Point(pnl_action.Width * 0.0625, pnl_action.Height * 0.375)
        btn_reinitialisation.Size = New Size(pnl_action.Width * 0.875, pnl_action.Height * 0.25)
        btn_echangetuile.Location = New Point(pnl_action.Width * 0.0625, pnl_action.Height * 0.6875)
        btn_echangetuile.Size = New Size(pnl_action.Width * 0.875, pnl_action.Height * 0.25)

        'panel d'infos
        pnl_info.Location = New Point(0, 0)
        pnl_info.Size = New Size(pnl_main.Width, pnl_main.Height * 0.2)
        lbl_nomjoueuractuel.Location = New Point(5, 5)
        lbl_tuilesrestantes.Location = New Point(pnl_info.Width - lbl_tuilesrestantes.Width - 5, 5)

        'Picturebox de la main
        For i = 1 To 6
            Dim pictureboxcreer As New PictureBox()
            pictureboxcreer.Name = "pct_main" & i
            pictureboxcreer.Location = New Point(pnl_main.Width * i * 0.04 + pnl_main.Width * (i - 1) * 0.12, pnl_main.Height * 0.4)
            pictureboxcreer.Size = New Size(pnl_main.Width * 0.08, pnl_action.Height * 0.4)
            pictureboxcreer.Image = imageTuileBlanche
            pictureboxcreer.SizeMode = PictureBoxSizeMode.Zoom
            pnl_main.Controls.Add(pictureboxcreer)
            AddHandler pictureboxcreer.MouseMove, AddressOf Pct_main_MouseMove
            AddHandler pictureboxcreer.Click, AddressOf pct_main_Click
        Next

        'Setup des panels d'affichage
        For i = 1 To 4
            Dim newpanel As New Panel
            newpanel.Name = "pnl_joueur" & i
            newpanel.Location = New Point(0.04 * pnl_affichagescore.Width, 0.02 * pnl_affichagescore.Height * i + (i - 1) * pnl_affichagescore.Height * 0.225)
            newpanel.Size = New Size(0.92 * pnl_affichagescore.Width, 0.225 * pnl_affichagescore.Height)
            Dim lbl_nomjoueur As New Label
            Dim lbl_scorejoueur As New Label
            lbl_nomjoueur.Name = "lbl_nomjoueur" & i
            lbl_nomjoueur.Text = "Joueur " & i
            lbl_nomjoueur.TextAlign = HorizontalAlignment.Center
            lbl_nomjoueur.Location = New Point(newpanel.Width / 2 - (lbl_nomjoueur.Width / 2), newpanel.Height * 0.2)
            lbl_scorejoueur.Name = "lbl_scorejoueur" & i
            lbl_scorejoueur.Text = "0 points"
            lbl_scorejoueur.TextAlign = HorizontalAlignment.Center
            lbl_scorejoueur.Location = New Point(newpanel.Width / 2 - (lbl_nomjoueur.Width / 2), newpanel.Height * 0.6)
            newpanel.Controls.Add(lbl_scorejoueur)
            newpanel.Controls.Add(lbl_nomjoueur)
            newpanel.Visible = False
            pnl_affichagescore.Controls.Add(newpanel)
        Next

        'Setup de la grille de jeu
        Dim coteGrille As Integer
        If (pnl_grilledejeu.Height > pnl_grilledejeu.Width) Then
            coteGrille = pnl_grilledejeu.Width
        Else
            coteGrille = pnl_grilledejeu.Height
        End If
        Dim panel_grille As New Panel
        panel_grille.Name = "pnl_grille"
        panel_grille.Location = New Point((pnl_grilledejeu.Width - coteGrille) / 2, 0)
        panel_grille.Size = New Size(coteGrille, coteGrille)
        panel_grille.BackColor = Color.White
        panel_grille.BorderStyle = 1
        panel_grille.AllowDrop = True
        pnl_grilledejeu.Controls.Add(panel_grille)


        For i = 1 To QwirklePartie.GetTailleGrille()
            For j = 1 To QwirklePartie.GetTailleGrille()
                Dim panel_caseGrille As New Panel
                panel_caseGrille.Name = "pnl_caseGrille" & i & "-" & j
                panel_caseGrille.Location = New Point(((i - 1) * coteGrille / QwirklePartie.GetTailleGrille()), ((j - 1) * coteGrille / QwirklePartie.GetTailleGrille()))
                panel_caseGrille.Size = New Size(coteGrille / QwirklePartie.GetTailleGrille(), coteGrille / QwirklePartie.GetTailleGrille())
                panel_caseGrille.BorderStyle = 1
                panel_grille.Controls.Add(panel_caseGrille)
                Dim pictureBoxCase As New PictureBox
                pictureBoxCase.Name = "pct_case" & i & "-" & j
                pictureBoxCase.Location = New Point(0, 0)
                pictureBoxCase.Size = New Size(panel_caseGrille.Width - 3, panel_caseGrille.Height - 3)
                pictureBoxCase.AllowDrop = False
                pictureBoxCase.SizeMode = PictureBoxSizeMode.StretchImage
                If (i = 7 And j = 7) Then
                    pictureBoxCase.BackColor = Color.Aquamarine
                    pictureBoxCase.AllowDrop = True
                End If
                panel_caseGrille.Controls.Add(pictureBoxCase)


                AddHandler pictureBoxCase.DragDrop, AddressOf CaseGrille_DragDrop
                AddHandler pictureBoxCase.DragEnter, AddressOf CaseGrille_DragEnter
            Next
        Next


        'Boutons d'echange desactivés
        btn_echangetuile.Enabled = False

    End Sub





    Private Sub frm_Qwirkle_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        'Affiche le formulaire de configuration de partie au premier plan

        frm_config.TopMost = True
        frm_config.ShowDialog()


        Dim label_nomjoueur As Label
        Dim panel_affichagejoueur As Panel
        For i = 1 To QwirklePartie.GetNombreJoueur()
            panel_affichagejoueur = pnl_affichagescore.Controls("pnl_joueur" & i)
            panel_affichagejoueur.Visible = True
            label_nomjoueur = panel_affichagejoueur.Controls("lbl_nomjoueur" & i)
            label_nomjoueur.Text = QwirklePartie.GetNomJoueur(i)
        Next
        QwirklePartie.DistributionTuile()
        QwirklePartie.RecherchePremierJoueur()
        ActualisationInfos()
    End Sub





    'Redimensionnement de la fenêtre
    Private Sub frm_Qwirkle_SizeChanged(sender As Object, e As EventArgs) Handles MyBase.SizeChanged

        Dim modulolargeur As Integer
        Dim modulohauteur As Integer
        modulolargeur = (Me.ClientSize.Width * 0.7) Mod 30
        modulohauteur = (Me.ClientSize.Width * 0.66) Mod 30

        'Redimensionnement des contrôles
        pnl_grilledejeu.Location = New Point(0, 0)
        pnl_grilledejeu.Size = New Size(Me.ClientSize.Width * 0.7 - modulolargeur, Me.ClientSize.Height * 0.8 - modulohauteur)
        pnl_affichagescore.Location = New Point(pnl_grilledejeu.Width, 0)
        pnl_affichagescore.Size = New Size(Me.ClientSize.Width - pnl_grilledejeu.Width, pnl_grilledejeu.Height)
        pnl_main.Location = New Point(0, pnl_grilledejeu.Height)
        pnl_main.Size = New Size(pnl_grilledejeu.Width, Me.ClientSize.Height - pnl_grilledejeu.Height)
        pnl_action.Location = New Point(pnl_grilledejeu.Width, pnl_grilledejeu.Height)
        pnl_action.Size = New Size(Me.ClientSize.Width - pnl_grilledejeu.Width, Me.ClientSize.Height - pnl_grilledejeu.Height)

        'Redimensionnement boutons panel action
        btn_terminertour.Location = New Point(pnl_action.Width * 0.0625, pnl_action.Height * 0.0625)
        btn_terminertour.Size = New Size(pnl_action.Width * 0.875, pnl_action.Height * 0.25)
        btn_reinitialisation.Location = New Point(pnl_action.Width * 0.0625, pnl_action.Height * 0.375)
        btn_reinitialisation.Size = New Size(pnl_action.Width * 0.875, pnl_action.Height * 0.25)
        btn_echangetuile.Location = New Point(pnl_action.Width * 0.0625, pnl_action.Height * 0.6875)
        btn_echangetuile.Size = New Size(pnl_action.Width * 0.875, pnl_action.Height * 0.25)

        'panel d'infos
        pnl_info.Location = New Point(0, 0)
        pnl_info.Size = New Size(pnl_main.Width, pnl_main.Height * 0.2)
        lbl_nomjoueuractuel.Location = New Point(5, 5)
        lbl_tuilesrestantes.Location = New Point(pnl_info.Width - lbl_tuilesrestantes.Width - 5, 5)

        'Picturebox de la main
        If pnl_main.Controls.Count > 1 Then
            For i = 1 To 6
                Dim pictureboxmain As PictureBox
                pictureboxmain = pnl_main.Controls("pct_main" & i)
                pictureboxmain.Location = New Point(pnl_main.Width * i * 0.04 + pnl_main.Width * (i - 1) * 0.12, pnl_main.Height * 0.4)
                pictureboxmain.Size = New Size(pnl_main.Width * 0.08, pnl_action.Height * 0.4)
            Next
        End If

        'Redimensionnement affichage score
        If (pnl_affichagescore.Controls.Count > 0) Then
            For i = 1 To 4
                Dim panelaffichage As Panel
                panelaffichage = pnl_affichagescore.Controls("pnl_joueur" & i)
                panelaffichage.Location = New Point(0.04 * pnl_affichagescore.Width, 0.02 * pnl_affichagescore.Height * i + (i - 1) * pnl_affichagescore.Height * 0.225)
                panelaffichage.Size = New Size(0.92 * pnl_affichagescore.Width, 0.225 * pnl_affichagescore.Height)
                Dim lbl_nomjoueur As Label
                Dim lbl_scorejoueur As Label
                lbl_nomjoueur = panelaffichage.Controls("lbl_nomjoueur" & i)
                lbl_scorejoueur = panelaffichage.Controls("lbl_scorejoueur" & i)
                lbl_nomjoueur.Location = New Point(panelaffichage.Width / 2 - (lbl_nomjoueur.Width / 2), panelaffichage.Height * 0.2)
                lbl_scorejoueur.Location = New Point(panelaffichage.Width / 2 - (lbl_nomjoueur.Width / 2), panelaffichage.Height * 0.6)
            Next
        End If

        'Redimensionnement de la grille de jeu
        Dim coteGrille As Integer
        If (pnl_grilledejeu.Height > pnl_grilledejeu.Width) Then
            coteGrille = pnl_grilledejeu.Width
        Else
            coteGrille = pnl_grilledejeu.Height
        End If
        If pnl_grilledejeu.Controls.Count > 0 Then
            Dim panel_grille As Panel
            panel_grille = pnl_grilledejeu.Controls("pnl_grille")
            panel_grille.Location = New Point((pnl_grilledejeu.Width - coteGrille) / 2, 0)
            panel_grille.Size = New Size(coteGrille, coteGrille)
            panel_grille.BackColor = Color.White
            panel_grille.BorderStyle = 1
            pnl_grilledejeu.Controls.Add(panel_grille)


            For i = 1 To QwirklePartie.GetTailleGrille()
                For j = 1 To QwirklePartie.GetTailleGrille()
                    Dim panel_caseGrille As Panel
                    panel_caseGrille = panel_grille.Controls("pnl_caseGrille" & i & "-" & j)
                    panel_caseGrille.Location = New Point(((i - 1) * coteGrille / QwirklePartie.GetTailleGrille()), ((j - 1) * coteGrille / QwirklePartie.GetTailleGrille()))
                    panel_caseGrille.Size = New Size(coteGrille / QwirklePartie.GetTailleGrille(), coteGrille / QwirklePartie.GetTailleGrille())
                    panel_caseGrille.BorderStyle = 1
                    panel_grille.Controls.Add(panel_caseGrille)
                    Dim pictureBoxCase As PictureBox
                    pictureBoxCase = panel_caseGrille.Controls("pct_case" & i & "-" & j)
                    pictureBoxCase.Size = New Size(panel_caseGrille.Width, panel_caseGrille.Height)

                Next
            Next

        End If

    End Sub


    Sub ActualisationInfos()
        Dim picturebox_main As PictureBox
        For i = 1 To 6
            picturebox_main = pnl_main.Controls("pct_main" & i)
            picturebox_main.Image = ImageTuile(QwirklePartie.GetCouleurTuileMainJoueur(i), QwirklePartie.GetSymboleTuileMainJoueur(i))
            picturebox_main.BorderStyle = 0
        Next
        lbl_nomjoueuractuel.Text = "Joueur actuel : " & QwirklePartie.GetNomJoueurActuel()
        lbl_tuilesrestantes.Text = QwirklePartie.GetNombreTuileRestantePioche() & " tuiles restantes"
    End Sub

    Sub ActualisationGrille()
        If (QwirklePartie.ActualisationTailleGrille()) Then
            Dim coteGrille As Integer
            If (pnl_grilledejeu.Height > pnl_grilledejeu.Width) Then
                coteGrille = pnl_grilledejeu.Width
            Else
                coteGrille = pnl_grilledejeu.Height
            End If

            Dim panel_grille As Panel
            panel_grille = pnl_grilledejeu.Controls("pnl_grille")
            panel_grille.Location = New Point((pnl_grilledejeu.Width - coteGrille) / 2, 0)
            panel_grille.Size = New Size(coteGrille, coteGrille)
            panel_grille.BackColor = Color.White
            panel_grille.BorderStyle = 1

            For i = 1 To QwirklePartie.GetAncienneTailleGrille()
                For j = 1 To QwirklePartie.GetAncienneTailleGrille()
                    Dim panel_caseGrille As Panel
                    panel_caseGrille = panel_grille.Controls("pnl_caseGrille" & i & "-" & j)
                    If (panel_caseGrille IsNot Nothing) Then
                        panel_caseGrille.Location = New Point(((i - 1) * coteGrille / QwirklePartie.GetTailleGrille()), ((j - 1) * coteGrille / QwirklePartie.GetTailleGrille()))
                        panel_caseGrille.Size = New Size(coteGrille / QwirklePartie.GetTailleGrille(), coteGrille / QwirklePartie.GetTailleGrille())
                        panel_caseGrille.BorderStyle = 1
                        panel_grille.Controls.Add(panel_caseGrille)
                        Dim pictureBoxCase As PictureBox
                        pictureBoxCase = panel_caseGrille.Controls("pct_case" & i & "-" & j)
                        pictureBoxCase.Size = New Size(panel_caseGrille.Width, panel_caseGrille.Height)
                        If (i = 7 And j = 7) Then
                            pictureBoxCase.BackColor = Color.White
                        End If
                        pictureBoxCase.SizeMode = PictureBoxSizeMode.StretchImage
                        pictureBoxCase.Image = ImageTuile(QwirklePartie.GetCouleurTuileGrille(j, i), QwirklePartie.GetSymboleTuileGrille(j, i))
                    End If
                Next
            Next

            For i = 1 To QwirklePartie.GetTailleGrille()
                For j = 1 To QwirklePartie.GetTailleGrille()
                    If (i > QwirklePartie.GetAncienneTailleGrille() Or j > QwirklePartie.GetAncienneTailleGrille()) Then
                        Dim panel_caseGrille As New Panel
                        panel_caseGrille.Name = "pnl_caseGrille" & i & "-" & j
                        panel_caseGrille.Location = New Point(((i - 1) * coteGrille / QwirklePartie.GetTailleGrille()), ((j - 1) * coteGrille / QwirklePartie.GetTailleGrille()))
                        panel_caseGrille.Size = New Size(coteGrille / QwirklePartie.GetTailleGrille(), coteGrille / QwirklePartie.GetTailleGrille())
                        panel_caseGrille.BorderStyle = 1
                        panel_grille.Controls.Add(panel_caseGrille)
                        Dim pictureBoxCase As New PictureBox
                        pictureBoxCase.Name = "pct_case" & i & "-" & j
                        pictureBoxCase.Location = New Point(0, 0)
                        pictureBoxCase.Size = New Size(panel_caseGrille.Width - 3, panel_caseGrille.Height - 3)
                        pictureBoxCase.AllowDrop = False
                        pictureBoxCase.SizeMode = PictureBoxSizeMode.StretchImage
                        panel_caseGrille.Controls.Add(pictureBoxCase)


                        AddHandler pictureBoxCase.DragDrop, AddressOf CaseGrille_DragDrop
                        AddHandler pictureBoxCase.DragEnter, AddressOf CaseGrille_DragEnter
                    End If
                Next
            Next
        Else
            Dim coteGrille As Integer
            If (pnl_grilledejeu.Height > pnl_grilledejeu.Width) Then
                coteGrille = pnl_grilledejeu.Width
            Else
                coteGrille = pnl_grilledejeu.Height
            End If

            Dim panel_grille As Panel
            panel_grille = pnl_grilledejeu.Controls("pnl_grille")
            panel_grille.Location = New Point((pnl_grilledejeu.Width - coteGrille) / 2, 0)
            panel_grille.Size = New Size(coteGrille, coteGrille)
            panel_grille.BackColor = Color.White
            panel_grille.BorderStyle = 1

            For i = 1 To QwirklePartie.GetTailleGrille()
                For j = 1 To QwirklePartie.GetTailleGrille()
                    Dim panel_caseGrille As Panel
                    panel_caseGrille = panel_grille.Controls("pnl_caseGrille" & i & "-" & j)
                    If (panel_caseGrille IsNot Nothing) Then
                        panel_caseGrille.Location = New Point(((i - 1) * coteGrille / QwirklePartie.GetTailleGrille()), ((j - 1) * coteGrille / QwirklePartie.GetTailleGrille()))
                        panel_caseGrille.Size = New Size(coteGrille / QwirklePartie.GetTailleGrille(), coteGrille / QwirklePartie.GetTailleGrille())
                        panel_caseGrille.BorderStyle = 1
                        panel_grille.Controls.Add(panel_caseGrille)
                        Dim pictureBoxCase As PictureBox
                        pictureBoxCase = panel_caseGrille.Controls("pct_case" & i & "-" & j)
                        pictureBoxCase.Size = New Size(panel_caseGrille.Width, panel_caseGrille.Height)
                        pictureBoxCase.Image = ImageTuile(QwirklePartie.GetCouleurTuileGrille(j, i), QwirklePartie.GetSymboleTuileGrille(j, i))
                    End If

                Next
            Next
        End If


    End Sub


    'Bouton terminerletour
    Private Sub Btn_terminertour_Click(sender As Object, e As EventArgs) Handles btn_terminertour.Click
        If (QwirklePartie.VerificationAucuneTuilePlacee) Then

        Else
            Dim panel_affichagejoueur As Panel
            panel_affichagejoueur = pnl_affichagescore.Controls("pnl_joueur" & QwirklePartie.GetIndiceJoueurActuel())
            Dim label_score As Label
            label_score = panel_affichagejoueur.Controls("lbl_scorejoueur" & QwirklePartie.GetIndiceJoueurActuel())
            label_score.Text = QwirklePartie.CalculDuScore()
            QwirklePartie.PiocheTuile()
            ActualisationGrille()
            QwirklePartie.ResetGrilleTourDuJoueur()
        End If

        If (QwirklePartie.VerificationFinDePartie) Then
            frm_ClassementFin.TopMost = True
            frm_ClassementFin.ShowDialog()

            QwirklePartie.Reinitialisation()

            frm_config.TopMost = True
            frm_config.ShowDialog()


            Dim label_nomjoueur As Label
            Dim panel_affichagejoueur As Panel
            For i = 1 To QwirklePartie.GetNombreJoueur()
                panel_affichagejoueur = pnl_affichagescore.Controls("pnl_joueur" & i)
                panel_affichagejoueur.Visible = True
                label_nomjoueur = panel_affichagejoueur.Controls("lbl_nomjoueur" & i)
                label_nomjoueur.Text = QwirklePartie.GetNomJoueur(i)
            Next
            QwirklePartie.DistributionTuile()
            QwirklePartie.RecherchePremierJoueur()
            Dim panel_grille As Panel
            panel_grille = pnl_grilledejeu.Controls("pnl_grille")
            panel_grille.Controls.Clear()
            Dim coteGrille As Integer
            If (pnl_grilledejeu.Height > pnl_grilledejeu.Width) Then
                coteGrille = pnl_grilledejeu.Width
            Else
                coteGrille = pnl_grilledejeu.Height
            End If
            For i = 1 To QwirklePartie.GetTailleGrille()
                For j = 1 To QwirklePartie.GetTailleGrille()
                    Dim panel_caseGrille As New Panel
                    panel_caseGrille.Name = "pnl_caseGrille" & i & "-" & j
                    panel_caseGrille.Location = New Point(((i - 1) * coteGrille / QwirklePartie.GetTailleGrille()), ((j - 1) * coteGrille / QwirklePartie.GetTailleGrille()))
                    panel_caseGrille.Size = New Size(coteGrille / QwirklePartie.GetTailleGrille(), coteGrille / QwirklePartie.GetTailleGrille())
                    panel_caseGrille.BorderStyle = 1
                    panel_grille.Controls.Add(panel_caseGrille)
                    Dim pictureBoxCase As New PictureBox
                    pictureBoxCase.Name = "pct_case" & i & "-" & j
                    pictureBoxCase.Location = New Point(0, 0)
                    pictureBoxCase.Size = New Size(panel_caseGrille.Width - 3, panel_caseGrille.Height - 3)
                    pictureBoxCase.AllowDrop = False
                    pictureBoxCase.SizeMode = PictureBoxSizeMode.StretchImage
                    If (i = 7 And j = 7) Then
                        pictureBoxCase.BackColor = Color.Aquamarine
                        pictureBoxCase.AllowDrop = True
                    End If
                    panel_caseGrille.Controls.Add(pictureBoxCase)


                    AddHandler pictureBoxCase.DragDrop, AddressOf CaseGrille_DragDrop
                    AddHandler pictureBoxCase.DragEnter, AddressOf CaseGrille_DragEnter
                Next
            Next
            ActualisationInfos()
        Else
            QwirklePartie.JoueurSuivant()
            ActualisationInfos()
        End If

    End Sub

    'Bouton Réinitialiser le tour
    Private Sub Btn_reinitialisation_Click(sender As Object, e As EventArgs) Handles btn_reinitialisation.Click
        QwirklePartie.ResetSelectionTuileMain()
        QwirklePartie.ReinitialisationGrille()
        QwirklePartie.ResetPremierCoup()
        Dim panel_grille As Panel
        panel_grille = pnl_grilledejeu.Controls("pnl_grille")
        Dim panel_caseGrille As Panel
        panel_caseGrille = panel_grille.Controls("pnl_caseGrille7-7")
        Dim pictureBoxCase As PictureBox
        pictureBoxCase = panel_caseGrille.Controls("pct_case7-7")
        pictureBoxCase.BackColor = Color.Aquamarine
        pictureBoxCase.Image = Nothing
        ActualisationGrille()
        ActualisationInfos()
        btn_echangetuile.Enabled = False
        btn_terminertour.Enabled = True
        Dim pic As PictureBox
        For i = 1 To 6
            pic = pnl_main.Controls("pct_main" & i)
            If pic.Name Like "pct_main" & i Then
                pic.BorderStyle = 0
            End If
        Next

    End Sub

    'Bouton d'echange de tuile
    Private Sub Btn_echangetuile_Click(sender As Object, e As EventArgs) Handles btn_echangetuile.Click
        QwirklePartie.EchangeTuile()
        QwirklePartie.ResetSelectionTuileMain()
        QwirklePartie.JoueurSuivant()
        ActualisationInfos()
        btn_echangetuile.Enabled = False
        btn_terminertour.Enabled = True
    End Sub

    'Selection au clic de la main
    Private Sub Pct_main_Click(sender As Object, e As EventArgs)
        If (QwirklePartie.GetNombreTuilePose = 0) Then
            Dim pic As PictureBox = sender
            For i = 1 To 6
                If pic.Name Like "pct_main" & i Then
                    If (QwirklePartie.VerificationSelectionTuileMain(i)) Then
                        pic.BorderStyle = 0
                        QwirklePartie.DeselectionTuileMain(i)
                    Else
                        pic.BorderStyle = 2
                        QwirklePartie.SelectionTuileMain(i)
                    End If
                End If
            Next
            If (QwirklePartie.VerificationSelectionActive) Then
                btn_terminertour.Enabled = False
                btn_echangetuile.Enabled = True
            Else
                btn_echangetuile.Enabled = False
                btn_terminertour.Enabled = True
            End If
        End If
    End Sub

    Dim indiceTuileMain As Integer


    'Drag And Drop Mouse Move
    Private Sub Pct_main_MouseMove(sender As Object, e As MouseEventArgs)
        Dim pic As PictureBox = sender
        Dim effect As DragDropEffects
        If e.Button = MouseButtons.Left Then
            effect = pic.DoDragDrop(pic.Image, DragDropEffects.Move)
            If (effect = DragDropEffects.Move) Then
                pic.Image = imageTuileBlanche
            End If
        End If
        indiceTuileMain = Convert.ToInt32(pic.Name(pic.Name.Length - 1)) - 48
        For i = 1 To QwirklePartie.GetTailleGrille()
            For j = 1 To QwirklePartie.GetTailleGrille()
                Dim panel_grille As Panel
                panel_grille = pnl_grilledejeu.Controls("pnl_grille")
                Dim panel_caseGrille As Panel
                panel_caseGrille = panel_grille.Controls("pnl_caseGrille" & i & "-" & j)
                Dim pictureBoxCase As PictureBox
                pictureBoxCase = panel_caseGrille.Controls("pct_case" & i & "-" & j)
                Dim pictureBox As PictureBox = sender

                If (QwirklePartie.AutorisationPlacementTuile(indiceTuileMain - 1, j, i)) Then
                    pictureBoxCase.AllowDrop = True
                Else
                    pictureBoxCase.AllowDrop = False
                End If
            Next
        Next
    End Sub

    'Drag and Drop DragDrop
    Private Sub CaseGrille_DragDrop(sender As Object, e As DragEventArgs)
        Dim pictureBox As PictureBox = sender
        Dim colonne As Integer
        Dim ligne As Integer
        pictureBox.Image = e.Data.GetData(DataFormats.Bitmap)
        If (pictureBox.Name(pictureBox.Name.Length - 2) = "-") Then
            colonne = Convert.ToInt32(pictureBox.Name(pictureBox.Name.Length - 1)) - 48
            If (pictureBox.Name(pictureBox.Name.Length - 4) = "e") Then
                ligne = Convert.ToInt32(pictureBox.Name(pictureBox.Name.Length - 3)) - 48
            Else
                ligne = (Convert.ToInt32(pictureBox.Name(pictureBox.Name.Length - 4)) - 48) * 10 + (Convert.ToInt32(pictureBox.Name(pictureBox.Name.Length - 3)) - 48)
            End If
        Else
            colonne = (Convert.ToInt32(pictureBox.Name(pictureBox.Name.Length - 2)) - 48) * 10 + (Convert.ToInt32(pictureBox.Name(pictureBox.Name.Length - 1)) - 48)
            If (pictureBox.Name(pictureBox.Name.Length - 5) = "e") Then
                ligne = Convert.ToInt32(pictureBox.Name(pictureBox.Name.Length - 4)) - 48
            Else
                ligne = (Convert.ToInt32(pictureBox.Name(pictureBox.Name.Length - 5)) - 48) * 10 + (Convert.ToInt32(pictureBox.Name(pictureBox.Name.Length - 4)) - 48)
            End If
        End If

        QwirklePartie.PlacementTuile(indiceTuileMain - 1, colonne, ligne)
        btn_echangetuile.Enabled = False
    End Sub

    'Drag and Drop DragEnter
    Private Sub CaseGrille_DragEnter(sender As Object, e As DragEventArgs)
        If e.Data.GetDataPresent(DataFormats.Bitmap) Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If

    End Sub

End Class
