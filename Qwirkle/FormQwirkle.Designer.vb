﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_Qwirkle
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.pnl_action = New System.Windows.Forms.Panel()
        Me.btn_reinitialisation = New System.Windows.Forms.Button()
        Me.btn_echangetuile = New System.Windows.Forms.Button()
        Me.btn_terminertour = New System.Windows.Forms.Button()
        Me.pnl_affichagescore = New System.Windows.Forms.Panel()
        Me.pnl_main = New System.Windows.Forms.Panel()
        Me.pnl_info = New System.Windows.Forms.Panel()
        Me.lbl_tuilesrestantes = New System.Windows.Forms.Label()
        Me.lbl_nomjoueuractuel = New System.Windows.Forms.Label()
        Me.pnl_grilledejeu = New System.Windows.Forms.Panel()
        Me.pnl_action.SuspendLayout()
        Me.pnl_main.SuspendLayout()
        Me.pnl_info.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnl_action
        '
        Me.pnl_action.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.pnl_action.Controls.Add(Me.btn_reinitialisation)
        Me.pnl_action.Controls.Add(Me.btn_echangetuile)
        Me.pnl_action.Controls.Add(Me.btn_terminertour)
        Me.pnl_action.Location = New System.Drawing.Point(700, 400)
        Me.pnl_action.Name = "pnl_action"
        Me.pnl_action.Size = New System.Drawing.Size(280, 150)
        Me.pnl_action.TabIndex = 0
        '
        'btn_reinitialisation
        '
        Me.btn_reinitialisation.AutoSize = True
        Me.btn_reinitialisation.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold)
        Me.btn_reinitialisation.Location = New System.Drawing.Point(10, 75)
        Me.btn_reinitialisation.Margin = New System.Windows.Forms.Padding(10)
        Me.btn_reinitialisation.Name = "btn_reinitialisation"
        Me.btn_reinitialisation.Size = New System.Drawing.Size(254, 31)
        Me.btn_reinitialisation.TabIndex = 2
        Me.btn_reinitialisation.Text = "Réinitialiser mon tour"
        Me.btn_reinitialisation.UseVisualStyleBackColor = True
        '
        'btn_echangetuile
        '
        Me.btn_echangetuile.AutoSize = True
        Me.btn_echangetuile.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold)
        Me.btn_echangetuile.Location = New System.Drawing.Point(10, 115)
        Me.btn_echangetuile.Margin = New System.Windows.Forms.Padding(10)
        Me.btn_echangetuile.Name = "btn_echangetuile"
        Me.btn_echangetuile.Size = New System.Drawing.Size(254, 31)
        Me.btn_echangetuile.TabIndex = 1
        Me.btn_echangetuile.Text = "Echanger des tuiles"
        Me.btn_echangetuile.UseVisualStyleBackColor = True
        '
        'btn_terminertour
        '
        Me.btn_terminertour.AutoSize = True
        Me.btn_terminertour.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_terminertour.Location = New System.Drawing.Point(10, 14)
        Me.btn_terminertour.Margin = New System.Windows.Forms.Padding(0)
        Me.btn_terminertour.Name = "btn_terminertour"
        Me.btn_terminertour.Size = New System.Drawing.Size(252, 51)
        Me.btn_terminertour.TabIndex = 0
        Me.btn_terminertour.Text = "Terminer le tour"
        Me.btn_terminertour.UseVisualStyleBackColor = True
        '
        'pnl_affichagescore
        '
        Me.pnl_affichagescore.BackColor = System.Drawing.SystemColors.Info
        Me.pnl_affichagescore.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnl_affichagescore.Location = New System.Drawing.Point(700, 0)
        Me.pnl_affichagescore.Name = "pnl_affichagescore"
        Me.pnl_affichagescore.Size = New System.Drawing.Size(280, 400)
        Me.pnl_affichagescore.TabIndex = 1
        '
        'pnl_main
        '
        Me.pnl_main.BackColor = System.Drawing.SystemColors.Window
        Me.pnl_main.Controls.Add(Me.pnl_info)
        Me.pnl_main.Location = New System.Drawing.Point(0, 399)
        Me.pnl_main.Name = "pnl_main"
        Me.pnl_main.Size = New System.Drawing.Size(700, 150)
        Me.pnl_main.TabIndex = 2
        '
        'pnl_info
        '
        Me.pnl_info.BackColor = System.Drawing.SystemColors.Window
        Me.pnl_info.Controls.Add(Me.lbl_tuilesrestantes)
        Me.pnl_info.Controls.Add(Me.lbl_nomjoueuractuel)
        Me.pnl_info.Location = New System.Drawing.Point(0, 0)
        Me.pnl_info.Name = "pnl_info"
        Me.pnl_info.Size = New System.Drawing.Size(700, 43)
        Me.pnl_info.TabIndex = 0
        '
        'lbl_tuilesrestantes
        '
        Me.lbl_tuilesrestantes.AutoSize = True
        Me.lbl_tuilesrestantes.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!)
        Me.lbl_tuilesrestantes.Location = New System.Drawing.Point(538, 15)
        Me.lbl_tuilesrestantes.Name = "lbl_tuilesrestantes"
        Me.lbl_tuilesrestantes.Size = New System.Drawing.Size(156, 20)
        Me.lbl_tuilesrestantes.TabIndex = 1
        Me.lbl_tuilesrestantes.Text = "180 tuiles restantes"
        '
        'lbl_nomjoueuractuel
        '
        Me.lbl_nomjoueuractuel.AutoSize = True
        Me.lbl_nomjoueuractuel.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nomjoueuractuel.Location = New System.Drawing.Point(12, 15)
        Me.lbl_nomjoueuractuel.Name = "lbl_nomjoueuractuel"
        Me.lbl_nomjoueuractuel.Size = New System.Drawing.Size(96, 20)
        Me.lbl_nomjoueuractuel.TabIndex = 0
        Me.lbl_nomjoueuractuel.Text = "Au tour de :"
        '
        'pnl_grilledejeu
        '
        Me.pnl_grilledejeu.AllowDrop = True
        Me.pnl_grilledejeu.BackColor = System.Drawing.SystemColors.ControlDark
        Me.pnl_grilledejeu.Location = New System.Drawing.Point(0, 0)
        Me.pnl_grilledejeu.Name = "pnl_grilledejeu"
        Me.pnl_grilledejeu.Size = New System.Drawing.Size(700, 400)
        Me.pnl_grilledejeu.TabIndex = 3
        '
        'frm_Qwirkle
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(982, 553)
        Me.Controls.Add(Me.pnl_grilledejeu)
        Me.Controls.Add(Me.pnl_main)
        Me.Controls.Add(Me.pnl_affichagescore)
        Me.Controls.Add(Me.pnl_action)
        Me.MinimumSize = New System.Drawing.Size(1000, 600)
        Me.Name = "frm_Qwirkle"
        Me.Text = "Qwirkle"
        Me.pnl_action.ResumeLayout(False)
        Me.pnl_action.PerformLayout()
        Me.pnl_main.ResumeLayout(False)
        Me.pnl_info.ResumeLayout(False)
        Me.pnl_info.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents pnl_action As Panel
    Friend WithEvents btn_reinitialisation As Button
    Friend WithEvents btn_echangetuile As Button
    Friend WithEvents btn_terminertour As Button
    Friend WithEvents pnl_affichagescore As Panel
    Friend WithEvents pnl_main As Panel
    Friend WithEvents pnl_grilledejeu As Panel
    Friend WithEvents pnl_info As Panel
    Friend WithEvents lbl_nomjoueuractuel As Label
    Friend WithEvents lbl_tuilesrestantes As Label
End Class
