﻿Imports QwirkleLib

Public Class frm_config
    'Bouton Quitter
    Private Sub btn_configQuitter_Click(sender As Object, e As EventArgs) Handles btn_configQuitter.Click
        End
    End Sub

    Private Sub btn_configCommencer_Click(sender As Object, e As EventArgs) Handles btn_configCommencer.Click
        Dim textbox As TextBox
        Dim textbox2 As TextBox
        Dim saisieincorrecte As Boolean
        saisieincorrecte = False

        For i = 1 To nmb_nbJoueurs.Value
            textbox = Me.Controls("txt_nomjoueur" & i)
            If (textbox.Text = vbNullString And saisieincorrecte = False) Then
                saisieincorrecte = True
                MessageBox.Show("La saisie du nom des joueurs est incorrecte : l'un des champs est vide")
            ElseIf (saisieincorrecte = False) Then
                'Mettre dans le tableaue de joueurs leurs noms
                QwirklePartie.SetNomJoueur(textbox.Text, i)
            End If
        Next

        For i = 1 To nmb_nbJoueurs.Value
            For j = 1 To nmb_nbJoueurs.Value
                If (j <> i) Then
                    textbox = Me.Controls("txt_nomjoueur" & i)
                    textbox2 = Me.Controls("txt_nomjoueur" & j)
                    If (textbox.Text = textbox2.Text And saisieincorrecte = False) Then
                        saisieincorrecte = True
                        MessageBox.Show("La saisie du nom des joueurs est incorrecte : deux champs ou plus sont identiques")
                    End If
                End If
            Next
        Next

        If (saisieincorrecte = False) Then
            'Initialisation de la Game
            QwirklePartie.SetNombreJoueur(nmb_nbJoueurs.Value)
            Me.Close()
        End If
    End Sub

    Private Sub nmb_nbJoueurs_ValueChanged(sender As Object, e As EventArgs) Handles nmb_nbJoueurs.ValueChanged
        Dim textbox As TextBox
        If (Me.Controls.Count > 5) Then
            For i = 1 To 4
                textbox = Me.Controls("txt_nomjoueur" & i)
                If (i <= nmb_nbJoueurs.Value) Then
                    textbox.Enabled = True
                Else
                    textbox.Enabled = False
                End If
            Next
        End If
    End Sub


End Class
