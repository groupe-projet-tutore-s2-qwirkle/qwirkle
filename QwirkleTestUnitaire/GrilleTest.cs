﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleLib;
using System.Windows.Forms;

namespace QwirkleTestUnitaire
{
    [TestClass]
    public class GrilleTest
    {
        [TestMethod]
        public void TestConstructeur()
        {
            Grille grilleTest = new Grille();
        }

        [TestMethod]
        public void TestPlacement()
        {
            Grille grilleTest = new Grille();
            Tuile tuile1 = new Tuile("bleu", "croix");
            Tuile tuile2 = new Tuile("bleu", "carre");
            Tuile tuile3 = new Tuile("bleu", "losange");
            Tuile tuile4 = new Tuile("bleu", "trefle");
            Tuile tuile5 = new Tuile("bleu", "cercle");
            Tuile tuile6 = new Tuile("bleu", "etoile");
            Assert.AreEqual(grilleTest.VerificationLegalitePlacement(tuile1, 7, 7), true);
            grilleTest = new Grille();
            Assert.AreEqual(grilleTest.VerificationLegalitePlacement(tuile1, 8, 7), false);
            grilleTest = new Grille();
            grilleTest.SetTuile(tuile1, 7, 7);
            Assert.AreEqual(grilleTest.VerificationLegalitePlacement(tuile2, 8, 7), true);
            grilleTest = new Grille();
            grilleTest.SetTuile(tuile1, 7, 7);
            Assert.AreEqual(grilleTest.VerificationLegalitePlacement(tuile1, 10, 7), false);
            grilleTest = new Grille();
            grilleTest.SetTuile(tuile1, 7, 7);
            grilleTest.SetTuile(tuile2, 8, 7);
            grilleTest.SetTuile(tuile3, 9, 7);
            grilleTest.SetTuile(tuile4, 10, 7);
            grilleTest.SetTuile(tuile5, 11, 7);
            Assert.AreEqual(grilleTest.VerificationLegalitePlacement(tuile6, 12, 7), true);
            grilleTest.SetTuile(tuile6, 12, 7);
            Assert.AreEqual(grilleTest.VerificationLegalitePlacement(tuile1, 13, 7), false);

        }


        [TestMethod]
        public void TestCalculScore()
        {
            Grille grilleTest = new Grille();
            Tuile tuile1 = new Tuile("bleu", "croix");
            Tuile tuile2 = new Tuile("bleu", "carre");
            Tuile tuile3 = new Tuile("bleu", "losange");
            Tuile tuile4 = new Tuile("bleu", "trefle");
            Tuile tuile5 = new Tuile("bleu", "cercle");
            Tuile tuile6 = new Tuile("bleu", "etoile");
            grilleTest.SetTuile(tuile1, 7, 7);
            grilleTest.SetTuile(tuile2, 8, 7);
            grilleTest.SetTuile(tuile3, 9, 7);
            grilleTest.SetTuile(tuile4, 10, 7);
            grilleTest.SetTuile(tuile5, 11, 7);
            Assert.AreEqual(grilleTest.CalculScore(), 5);
            grilleTest.SetTuile(tuile6, 12, 7);
            Assert.AreEqual(grilleTest.CalculScore(), 12);

        }

        [TestMethod]
        public void Test()
        {
            Grille grilleTest = new Grille();
            Tuile tuile1 = new Tuile("bleu", "croix");
            Tuile tuile2 = new Tuile("bleu", "carre");
            Tuile tuile3 = new Tuile("bleu", "losange");
            Tuile tuile4 = new Tuile("bleu", "trefle");
            Tuile tuile5 = new Tuile("bleu", "cercle");
            Tuile tuile6 = new Tuile("bleu", "etoile");
            grilleTest.SetTuile(tuile1, 7, 7);
            grilleTest.SetTuile(tuile2, 8, 7);
            grilleTest.SetTuile(tuile3, 9, 7);
            grilleTest.SetTuile(tuile4, 10, 7);
            grilleTest.SetTuile(tuile5, 11, 7);
            grilleTest.SetTuile(tuile1, 7, 8);
            grilleTest.SetTuile(tuile2, 7, 9);
            grilleTest.SetTuile(tuile3, 7, 10);
            grilleTest.SetTuile(tuile4, 7, 11);
            Assert.AreEqual(grilleTest.CalculScore(), 9);
        }



    }
}
