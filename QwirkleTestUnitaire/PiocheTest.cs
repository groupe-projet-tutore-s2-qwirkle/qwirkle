﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleLib;

namespace QwirkleTestUnitaire
{
    [TestClass]
    public class PiocheTest
    {
        [TestMethod]
        public void TestConstructeur()
        {
            Pioche piochedujeu = new Pioche();
            Assert.AreEqual(piochedujeu.GetNombreDeTuile(), 108);
        }

        [TestMethod]
        public void TestTuileAleatoire()
        {
            Pioche piochedujeu = new Pioche();
            Tuile tuile1 = new Tuile("test1", "test2");
            tuile1 = piochedujeu.GetTuileAleatoire();
            Tuile tuile2 = new Tuile("test1", "test2");
            tuile2 = piochedujeu.GetTuileAleatoire();
            Assert.AreNotEqual(tuile1.GetCouleur(), "test1");
            Assert.AreNotEqual(tuile1.GetCouleur(), "erreur");
            Assert.AreNotEqual(tuile1.GetSymbole(), "test2");
            Assert.AreNotEqual(tuile1.GetSymbole(), "erreur");
            Assert.AreNotEqual(tuile1.GetSymbole(), tuile2.GetSymbole());
            Assert.AreNotEqual(tuile1.GetCouleur(), tuile2.GetCouleur());
        }
    }
}
