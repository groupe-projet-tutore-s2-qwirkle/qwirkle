﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleLib;

namespace QwirkleTestUnitaire
{
    [TestClass]
    public class TuileTest
    {
        [TestMethod]
        public void TestConstructeur()
        {
            Tuile tuile1 = new Tuile("rouge", "croix");
            Assert.AreEqual("rouge", tuile1.GetCouleur());
            Assert.AreEqual("croix", tuile1.GetSymbole());
            Tuile tuile2 = new Tuile("bleu", "trefle");
            Assert.AreEqual("bleu", tuile2.GetCouleur());
            Assert.AreEqual("trefle", tuile2.GetSymbole());
            Tuile tuile3 = new Tuile();
            tuile3 = tuile2;
            Assert.AreEqual("bleu", tuile3.GetCouleur());
            Assert.AreEqual("trefle", tuile3.GetSymbole());
            Assert.AreEqual(tuile3 == tuile2, true);
            Assert.AreEqual(tuile3 == tuile1, false);

        }

        [TestMethod]
        public void TestGetSetSymbole()
        {
            Tuile tuile1 = new Tuile("rouge", "cercle");
            Assert.AreEqual("cercle", tuile1.GetSymbole());
            tuile1.SetSymbole("etoile");
            Assert.AreEqual("etoile", tuile1.GetSymbole());
            Tuile tuile2 = new Tuile("violet", "etoile");
            Assert.AreEqual("etoile", tuile1.GetSymbole());
            tuile1.SetSymbole("carre");
            Assert.AreEqual("carre", tuile1.GetSymbole());
        }

        [TestMethod]
        public void TestGetSetCouleur()
        {
            Tuile tuile1 = new Tuile("rouge", "cercle");
            Assert.AreEqual("rouge", tuile1.GetCouleur());
            tuile1.SetCouleur("bleu");
            Assert.AreEqual("bleu", tuile1.GetCouleur());
            Tuile tuile2 = new Tuile("violet", "cercle");
            Assert.AreEqual("violet", tuile2.GetCouleur());
            tuile2.SetCouleur("rouge");
            Assert.AreEqual("rouge", tuile2.GetCouleur());
        }

        [TestMethod]
        public void TestGetSetEtat()
        {
            Tuile tuile1 = new Tuile("rouge", "cercle");
            Assert.AreEqual(false, tuile1.GetEtat());
            tuile1.SetEtat(true);
            Assert.AreEqual(true, tuile1.GetEtat());
            tuile1.SetEtat(false);
            Assert.AreEqual(false, tuile1.GetEtat());
        }
    }
}
