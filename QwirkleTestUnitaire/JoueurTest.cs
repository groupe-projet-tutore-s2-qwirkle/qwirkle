﻿using System;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleLib;


namespace QwirkleTestUnitaire
{
    [TestClass]
    public class JoueurTest
    {
        [TestMethod]
        public void TestConstructeur()
        {
            Joueur joueur1 = new Joueur();
            Assert.AreEqual(joueur1.GetNom(), "");
            Assert.AreEqual(joueur1.GetScore(), 0);
            Tuile tuileMain = new Tuile("random", "random");
            tuileMain = joueur1.GetTuileMain(1);
            Assert.AreEqual(tuileMain.GetCouleur(), "defaut");
            Assert.AreEqual(tuileMain.GetSymbole(), "defaut");
            Assert.AreEqual(tuileMain.GetEtat(), false);
        }
            
        [TestMethod]
        public void TestSetNom()
        {
            Joueur joueur1 = new Joueur();
            joueur1.SetNom("RandomNomDeJoeur");
            Assert.AreEqual(joueur1.GetNom(), "RandomNomDeJoeur");
        }

        [TestMethod]
        public void TestSetTuile()
        {
            Tuile tuileTemp = new Tuile("TestSetTuileC", "TestSetTuileS");
            Joueur joueur1 = new Joueur();
            joueur1.SetTuileMain(tuileTemp, 0);
            Assert.AreEqual(tuileTemp.GetCouleur(), "TestSetTuileC");
            Assert.AreEqual(tuileTemp.GetSymbole(), "TestSetTuileS");
            Assert.AreEqual(tuileTemp.GetEtat(), false);
        }

        [TestMethod]
        public void TestScore()
        {
            Joueur joueur1 = new Joueur();
            Assert.AreEqual(joueur1.GetScore(), 0);
            joueur1.AjouterScore(150);
            Assert.AreEqual(joueur1.GetScore(), 150);
            joueur1.AjouterScore(3);
            Assert.AreEqual(joueur1.GetScore(), 153);
        }

        [TestMethod] 
        public void TestDetectionPremierJoueur()
        {
            Tuile Tuile1 = new Tuile("rouge","croix");
            Tuile Tuile2 = new Tuile("bleu","croix");
            Tuile Tuile3 = new Tuile("bleu","croix");
            Tuile Tuile4 = new Tuile("jaune","croix");
            Tuile Tuile5 = new Tuile("rouge","croix");
            Tuile Tuile6 = new Tuile("bleu","carre");
            Joueur joueur1 = new Joueur();
            joueur1.SetTuileMain(Tuile1, 0);
            joueur1.SetTuileMain(Tuile2, 1);
            joueur1.SetTuileMain(Tuile3, 2);
            joueur1.SetTuileMain(Tuile4, 3);
            joueur1.SetTuileMain(Tuile5, 4);
            joueur1.SetTuileMain(Tuile6, 5);
            Assert.AreEqual(joueur1.GetNombreCaractCommune(), 5);
            
        }

    }

}

