﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleLib;
using System.Windows.Forms;

namespace QwirkleTestUnitaire
{
    [TestClass]
    public class TestQwirkle
    {
        [TestMethod]
        public void SetGetNombreEtNomsDesJoueurs()
        {
            string nom;
            for (int i = 1; i < 5; i++)
            {
                nom = "test" + i;
                QwirklePartie.SetNomJoueur(nom, i);
                Assert.AreEqual(QwirklePartie.GetNomJoueur(i), nom);
            }

            QwirklePartie.SetNombreJoueur(2);
            Assert.AreEqual(QwirklePartie.GetNombreJoueur(), 2);

        }
        
        [TestMethod]
        public void TestDistribution()
        {
            QwirklePartie.DistributionTuile();
            Assert.AreNotEqual(QwirklePartie.GetCouleurTuileMainJoueur(1), "defaut");
            Assert.AreNotEqual(QwirklePartie.GetSymboleTuileMainJoueur(1), "defaut");
        }
    }
}