﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QwirkleLib;

namespace QwirkleLib
{
    public class Tuile
    {
        //Attributs
        private string symbole;
        private string couleur;
        private bool etat;

        //Constructeurs0
        public Tuile()
        {
            this.couleur = "defaut";
            this.symbole = "defaut";
            this.etat = false;
        }

        public Tuile(string couleur, string symbole)
        {
            this.couleur = couleur;
            this.symbole = symbole;
            this.etat = false;
        }


        //Methodes
        //Accesseurs
        public string GetSymbole()
        {
            return symbole;
        }

        public string GetCouleur()
        {
            return couleur;
        }

        public void SetSymbole(string symbole)
        {
            this.symbole = symbole;
        }

        public void SetCouleur(string couleur)
        {
            this.couleur = couleur;
        }

        public bool GetEtat()
        {
            return etat;
        }

        public void SetEtat(bool etat)
        {
            this.etat = etat;
        }

        //Surcharge
        public static bool operator ==(Tuile tuile1, Tuile tuile2)
        {
            return tuile1.couleur == tuile2.couleur && tuile1.symbole == tuile2.symbole;
        }

        public static bool operator !=(Tuile tuile1, Tuile tuile2)
        {
            return !(tuile1 == tuile2);
        }

        public override bool Equals(object obj)
        {
            return obj is Tuile && this == (Tuile)obj;

        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash ^= this.couleur.GetHashCode();
            hash ^= this.symbole.GetHashCode();
            return hash;
        }
    }
}