﻿using System.Windows.Forms;

namespace QwirkleLib
{
    public class Grille
    {

        //Attributs
        Tuile[,] grilleDeJeu = new Tuile[50, 50];
        int coordCoinHautGauche;
        int nombreTuilePose;
        bool premierTourPartie;
        bool alignementVertical;
        int tailleGrille;
        int ancienneTailleGrille;

        //Constructeur
        public Grille()
        {
            for (int colonne = 0; colonne < 50; colonne++)
            {
                for (int ligne = 0; ligne < 50; ligne++)
                {
                    grilleDeJeu[ligne, colonne] = new Tuile();
                    nombreTuilePose = 0;
                    tailleGrille = 13;
                    coordCoinHautGauche = 18;
                    premierTourPartie = true;
                }
            }
        }

        //Methodes
        //Accesseurs
        public Tuile GetTuile(int x, int y)
        {
            x = x + coordCoinHautGauche;
            y = y + coordCoinHautGauche;
            return grilleDeJeu[y, x];
        }

        public void SetTuile(Tuile tuilePlace, int x, int y)
        {
            x = x + coordCoinHautGauche;
            y = y + coordCoinHautGauche;
            grilleDeJeu[y, x] = new Tuile(tuilePlace.GetCouleur(), tuilePlace.GetSymbole());
            grilleDeJeu[y, x].SetEtat(true);
            if (x == 25 && y == 25)
                premierTourPartie = false;
            if (nombreTuilePose == 0)
            {
                if (grilleDeJeu[y - 1, x].GetCouleur() != "defaut" || grilleDeJeu[y + 1, x].GetCouleur() != "defaut")
                {
                    alignementVertical = false;
                }
                else if (grilleDeJeu[y, x - 1].GetCouleur() != "defaut" || grilleDeJeu[y, x + 1].GetCouleur() != "defaut")
                {
                    alignementVertical = true;
                }

            }
            if (nombreTuilePose == 1)
            {
                if (grilleDeJeu[y - 1, x].GetEtat() || grilleDeJeu[y + 1, x].GetEtat())
                {
                    alignementVertical = false;
                }
                else if (grilleDeJeu[y, x - 1].GetEtat() || grilleDeJeu[y, x + 1].GetEtat())
                {
                    alignementVertical = true;
                }

            }
            nombreTuilePose++;

        }

        public int GetNombreTuilePose()
        {
            return nombreTuilePose;
        }

        public int GetTaille()
        {
            return tailleGrille;
        }

        public int GetAncienneTaille()
        {
            return ancienneTailleGrille;
        }

        public string GetCouleurTuile(int x, int y)
        {
            x = x + coordCoinHautGauche;
            y = y + coordCoinHautGauche;
            return grilleDeJeu[y, x].GetCouleur();
        }

        public string GetSymboleTuile(int x, int y)
        {
            x = x + coordCoinHautGauche;
            y = y + coordCoinHautGauche;
            return grilleDeJeu[y, x].GetSymbole();
        }

        //Methodes d'extraction
        public Tuile[] ExtractionVerticale(int x, int y)
        {
            Tuile tuileDefaut = new Tuile();
            Tuile[] colonneTuile = null;
            int minimumY = y, maximumY = y;
            int indiceTableau = 0;
            if (grilleDeJeu[y, x] == tuileDefaut)
                return colonneTuile;
            while (grilleDeJeu[--minimumY, x].GetCouleur() != tuileDefaut.GetCouleur()) ;
            while (grilleDeJeu[++maximumY, x].GetCouleur() != tuileDefaut.GetCouleur()) ;
            colonneTuile = new Tuile[maximumY - minimumY - 1];
            for (int indiceY = minimumY + 1; indiceY < maximumY; indiceY++)
                colonneTuile[indiceTableau++] = grilleDeJeu[indiceY, x];
            return colonneTuile;
        }

        public Tuile[] ExtractionHorizontale(int x, int y)
        {
            Tuile tuileDefaut = new Tuile();
            Tuile[] ligneTuile = null;
            int minimumX = x, maximumX = x;
            int indiceTableau = 0;
            if (grilleDeJeu[y, x] == tuileDefaut)
                return ligneTuile;
            while (grilleDeJeu[y, --minimumX].GetCouleur() != tuileDefaut.GetCouleur()) ;
            while (grilleDeJeu[y, ++maximumX].GetCouleur() != tuileDefaut.GetCouleur()) ;
            ligneTuile = new Tuile[maximumX - minimumX - 1];
            for (int indiceX = minimumX + 1; indiceX < maximumX; indiceX++)
                ligneTuile[indiceTableau++] = grilleDeJeu[y, indiceX];
            return ligneTuile;
        }

        //Methodes de Verification de legalite de  Placement
        public bool VerificationCaseAdjacenteNonVide(int x, int y)
        {
            Tuile tuileDefaut = new Tuile();
            if (tuileDefaut != grilleDeJeu[y - 1, x] || tuileDefaut != grilleDeJeu[y, x - 1] || tuileDefaut != grilleDeJeu[y, x + 1] || tuileDefaut != grilleDeJeu[y + 1, x])
                return true;
            else
                return false;
        }

        public bool VerificationCaseAdjacentePlace(int x, int y)
        {
            if (grilleDeJeu[y - 1, x].GetEtat() || grilleDeJeu[y + 1, x].GetEtat() || grilleDeJeu[y, x - 1].GetEtat() || grilleDeJeu[y, x + 1].GetEtat())
                return true;
            else
                return false;

        }

        public bool VerificationLegalitePlacementVerticale(Tuile tuilePlace, int x, int y)
        {
            grilleDeJeu[y, x] = tuilePlace;
            Tuile[] colonneTuile = ExtractionVerticale(x, y);
            grilleDeJeu[y, x] = new Tuile();

            if (colonneTuile.Length == 1)
                return true;
            if (colonneTuile.Length > 6)
                return false;

            for (int indiceColonneTuile = 0; indiceColonneTuile < colonneTuile.Length; indiceColonneTuile++)
            {
                if (tuilePlace == colonneTuile[indiceColonneTuile])
                {
                    for (int indiceResteColonneTuile = indiceColonneTuile + 1; indiceResteColonneTuile < colonneTuile.Length; indiceResteColonneTuile++)
                    {
                        if (tuilePlace == colonneTuile[indiceResteColonneTuile])
                            return false;
                    }
                }

                if (tuilePlace.GetCouleur() != colonneTuile[indiceColonneTuile].GetCouleur() && tuilePlace.GetSymbole() != colonneTuile[indiceColonneTuile].GetSymbole())
                    return false;

            }
            return true;
        }

        public bool VerificationLegalitePlacementHorizontale(Tuile tuilePlace, int x, int y)
        {
            grilleDeJeu[y, x] = tuilePlace;
            Tuile[] ligneTuile = ExtractionHorizontale(x, y);
            grilleDeJeu[y, x] = new Tuile();

            if (ligneTuile.Length == 1)
                return true;
            if (ligneTuile.Length > 6)
                return false;

            for (int indiceLigneTuile = 0; indiceLigneTuile < ligneTuile.Length; indiceLigneTuile++)
            {
                if (tuilePlace == ligneTuile[indiceLigneTuile])
                {
                    for (int indiceResteLigneTuile = indiceLigneTuile + 1; indiceResteLigneTuile < ligneTuile.Length; indiceResteLigneTuile++)
                    {
                        if (tuilePlace == ligneTuile[indiceResteLigneTuile])
                            return false;
                    }
                }

                if (tuilePlace.GetCouleur() != ligneTuile[indiceLigneTuile].GetCouleur() && tuilePlace.GetSymbole() != ligneTuile[indiceLigneTuile].GetSymbole())
                    return false;
            }
            return true;
        }

        public bool VerificationLegalitePlacement(Tuile tuilePlace, int x, int y)
        {
            Tuile tuileDefaut = new Tuile();
            x = x + coordCoinHautGauche;
            y = y + coordCoinHautGauche;
            if (tuilePlace.GetEtat())
                return false;

            if (tuilePlace == tuileDefaut)
                return false;

            if (premierTourPartie && x == 25 && y == 25)
                return true;
            else if (premierTourPartie == false && x == 25 && y == 25)
                return false;

            if (tuileDefaut != grilleDeJeu[y, x])
                return false;

            if (nombreTuilePose == 0)
            {
                if (VerificationCaseAdjacenteNonVide(x, y) && VerificationLegalitePlacementHorizontale(tuilePlace, x, y) && VerificationLegalitePlacementVerticale(tuilePlace, x, y))
                    return true;
                else
                    return false;
            }
            else
            {
                if (nombreTuilePose > 1)
                {
                    if (alignementVertical)
                    {
                        if ((grilleDeJeu[y - 1, x].GetEtat() || grilleDeJeu[y + 1, x].GetEtat()) && VerificationCaseAdjacenteNonVide(x,y) && VerificationLegalitePlacementHorizontale(tuilePlace, x, y) && VerificationLegalitePlacementVerticale(tuilePlace, x, y))
                        {
                            return true;
                        }
                        else
                            return false;
                    }
                    else
                    {
                        if ((grilleDeJeu[y - 1, x].GetEtat() || grilleDeJeu[y + 1, x].GetEtat()) && VerificationCaseAdjacenteNonVide(x, y) && VerificationLegalitePlacementHorizontale(tuilePlace, x, y) && VerificationLegalitePlacementVerticale(tuilePlace, x, y))
                            return true;
                        else
                            return false;
                    }
                }
                else
                {
                    if (VerificationCaseAdjacentePlace(x, y) && VerificationLegalitePlacementHorizontale(tuilePlace, x, y) && VerificationLegalitePlacementVerticale(tuilePlace, x, y))
                    {
                        return true;
                    }
                    else
                        return false;
                }

            }
        }

        //Methodes du calcul du score
        public int CalculScore()
        {
            int score = 0;
            Tuile tuileDefaut = new Tuile();
            int stockLigne=0, stockColonne=0;
            if (nombreTuilePose == 0)
                return 0;
            else
            {
                if (alignementVertical)
                {
                    for (int ligne = 0; ligne < 50; ligne++)
                    {
                        for (int colonne = 0; colonne < 50; colonne++)
                        {
                            if (grilleDeJeu[colonne, ligne].GetEtat())
                            {
                                Tuile[] tuileColonne = ExtractionVerticale(ligne, colonne);
                                score += tuileColonne.Length;
                                if (!(grilleDeJeu[colonne -1, ligne].GetCouleur() != "defaut" || grilleDeJeu[colonne +1, ligne].GetCouleur() != "defaut"))
                                    score -= 1;
                                stockLigne = ligne;
                                stockColonne = colonne;
                                if (tuileColonne.Length == 6)
                                    score += 6;
                            }
                        }

                    }
                    Tuile[] tuileLigne = ExtractionHorizontale(stockLigne, stockColonne);
                    score += tuileLigne.Length;
                    if (tuileLigne.Length == 6)
                        score += 6;


                }
                else
                {
                    for (int ligne = 0; ligne < 50; ligne++)
                    {
                        for (int colonne = 0; colonne < 50; colonne++)
                        {
                            if (grilleDeJeu[colonne, ligne].GetEtat())
                            {
                                Tuile[] tuileLigne = ExtractionHorizontale(ligne, colonne);
                                score += tuileLigne.Length;
                                if(!(grilleDeJeu[colonne, ligne - 1].GetCouleur() != "defaut" || grilleDeJeu[colonne, ligne + 1].GetCouleur() != "defaut"))
                                    score -= 1;
                                stockLigne = ligne;
                                stockColonne = colonne;
                                if (tuileLigne.Length == 6)
                                    score += 6;
                            }
                        }

                    }
                    Tuile[] tuileColonne = ExtractionVerticale(stockLigne, stockColonne);
                    score += tuileColonne.Length;
                    if (tuileColonne.Length == 6)
                        score += 6;
                }

            }

            return score;
        }
        

        //Gestion de la grille pendant les tours
        public void GrilleResetTour()
        {
            nombreTuilePose = 0;
            for (int colonne = 0; colonne < 50; colonne++)
            {
                for (int ligne = 0; ligne < 50; ligne++)
                {
                    if (grilleDeJeu[colonne, ligne].GetEtat())
                    {
                        grilleDeJeu[colonne, ligne].SetEtat(false);
                    }
                        
                }
            }
        }

        public void GrilleReinitialisationTour()
        {
            nombreTuilePose = 0;
            for (int colonne = 0; colonne < 50; colonne++)
            {
                for (int ligne = 0; ligne < 50; ligne++)
                {
                    if (grilleDeJeu[colonne, ligne].GetEtat())
                    {
                        grilleDeJeu[colonne, ligne] = new Tuile();
                    }

                }
            }
        }

        //Pour le boutun reinitialisation du tour au premier tour
        public void ResetPremierCoup()
        {
            if(tailleGrille == 13 &&  grilleDeJeu[25,25] == new Tuile())
                premierTourPartie = true;
        }


        //Gestion dynamique de la grille
        public bool ActualisationGrille()
        {
            if(tailleGrille == 49 || grilleDeJeu[25,25] == new Tuile())
            {
                return false;
            }
            int minimumLigne = 0, maximumLigne = 50, minimumColonne = 0, maximumColonne = 30;
            for(int ligne=0; ligne<30; ligne++)
            {
                for(int colonne=0; colonne<50; colonne++)
                {
                    if (grilleDeJeu[colonne, ligne].GetCouleur() != "defaut")
                        maximumLigne = ligne;
                }
            }
            for (int colonne = 0; colonne < 50; colonne++)
            {
                for (int ligne = 0; ligne < 50; ligne++)
                {
                    if (grilleDeJeu[colonne, ligne].GetCouleur() != "defaut")
                        maximumColonne = colonne;
                }
            }
            for (int ligne = 49; ligne >= 0; ligne--)
            {
                for (int colonne = 0; colonne < 50; colonne++)
                {
                    if (grilleDeJeu[colonne, ligne].GetCouleur() != "defaut")
                        minimumLigne = ligne;
                }
            }
            for (int colonne = 49; colonne >=0; colonne--)
            {
                for (int ligne = 0; ligne < 50; ligne++)
                {
                    if (grilleDeJeu[colonne, ligne].GetCouleur() != "defaut")
                        minimumColonne = colonne;
                }
            }
            maximumColonne += 6;
            maximumLigne += 6;
            minimumColonne -= 6;
            minimumLigne -= 6;
            ancienneTailleGrille = tailleGrille;
            if (maximumColonne - minimumColonne > maximumLigne - minimumLigne)
            {
                if ((maximumColonne - minimumColonne) > tailleGrille)
                {
                    coordCoinHautGauche -= 1;
                    tailleGrille += 2;
                    return true;
                }
                else
                    return false;
                
            }                
            else
            {
                if ((maximumLigne - minimumLigne) > tailleGrille)
                {
                    coordCoinHautGauche -= 1;
                    tailleGrille += 2;
                    return true;
                }
                else
                    return false;

            }

        }
                

    }
}