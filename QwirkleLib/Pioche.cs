﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using QwirkleLib;

namespace QwirkleLib
{
    public class Pioche
    {
        //Attributs
        private List<Tuile> pioche = new List<Tuile>();
        
        //Constructeurs
        public Pioche()
        {
            string couleur, symbole;
            for (int tripleDeck = 0; tripleDeck<3; tripleDeck++)
            {
                for (int boucleColor = 0; boucleColor<6; boucleColor++)
                {
                    switch (boucleColor)
                    {
                        case 0:
                            couleur  = "jaune";
                            break;
                        case 1:
                            couleur = "rouge";
                            break;
                        case 2:
                            couleur = "vert";
                            break;
                        case 3:
                            couleur = "orange";
                            break;
                        case 4:
                            couleur = "violet";
                            break;
                        case 5:
                            couleur = "bleu";
                            break;
                        default:
                            couleur = "erreur";
                            break;
                    }
                    for (int boucleSymbole = 0; boucleSymbole<6; boucleSymbole++)
                    {
                        switch (boucleSymbole)
                        {
                            case 0:
                                symbole = "cercle";
                                break;
                            case 1:
                                symbole = "etoile";
                                break;
                            case 2:
                                symbole = "trefle";
                                break;
                            case 3:
                                symbole = "carre";
                                break;
                            case 4:
                                symbole = "losange";
                                break;
                            case 5:
                                symbole = "croix";
                                break;
                            default:
                                symbole = "erreur";
                                break;
                        }
                        pioche.Add(new Tuile(couleur, symbole));
                    }
                }
            }
        }

        //Methodes

        //Obtient une tuile alétaoire et l'enlève de la pioche
        public Tuile GetTuileAleatoire()
        {      
            Tuile tuilealeatoire = new Tuile("defaut", "dafaut");
            if (pioche.Count > 0)
            {
                Thread.Sleep(2);
                Random aleatoire = new Random(DateTime.Now.Millisecond);
                int entier;
                entier = aleatoire.Next(0, pioche.Count-1);
                tuilealeatoire.SetCouleur(pioche.ElementAt(entier).GetCouleur());
                tuilealeatoire.SetSymbole(pioche.ElementAt(entier).GetSymbole());
                pioche.RemoveAt(entier);
                return tuilealeatoire;
            }
            return tuilealeatoire;            
        }

        public void RemiseTuile(Tuile tuileRemise)
        {
            pioche.Add(tuileRemise);
        }

        //Accesseurs

        public int GetNombreDeTuile()
        {
            return pioche.Count;
        }


    }
}
