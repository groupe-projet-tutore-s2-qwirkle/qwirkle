﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QwirkleLib;

namespace QwirkleLib
{
    public class Joueur
    {
        //Attributs
        private int score;
        private string nom;
        private Tuile[] main = new Tuile[6];

        //Constructeurs
        public Joueur()
        {
            score = 0;
            nom = "";
            for(int indiceTuileMain = 0; indiceTuileMain < 6; indiceTuileMain++)
            {
                main[indiceTuileMain] = new Tuile();
            }
        }

        //Accesseurs
        public void SetNom(string nom)
        {
            this.nom = nom;
        }

        public string GetNom()
        {
            return nom;
        }

        public int GetScore()
        {
            return score;
        }

        public void SetTuileMain(Tuile tuileAjouter, int indiceTuileMain)
        {
            main[indiceTuileMain] = tuileAjouter;
        }

        public Tuile GetTuileMain(int indiceTuileMain)
        {
            return main[indiceTuileMain];
        }

        //Methodes
        public void AjouterScore(int ajout)
        {
            score += ajout;
        }


        //Méthode Selection Tuile
        public void SelectionTuile(int identifiantTuile)
        {
            main[identifiantTuile].SetEtat(true);
        }

        public void DeselectionTuile(int identifiantTuile)
        {
            main[identifiantTuile].SetEtat(false);
        }

        public bool VerificationSelectionTuile(int identifiantTuile)
        {
            return main[identifiantTuile].GetEtat();

        }

        public void ResetSelectionTuile()
        {
            for (int indiceTuileMain = 0; indiceTuileMain < 6; indiceTuileMain++)
            {
                main[indiceTuileMain].SetEtat(false);
            }
        }
        
        //Renvoi le nombre max de caractere commun qui se trouve dans la main du joueur
        public int GetNombreCaractCommune()
        {
            int[] occurenceCouleur = new int[]{0,0,0,0,0,0};
            int[] occurenceSymbole = new int[] {0,0,0,0,0,0};

            for (int indiceTuileMain = 0; indiceTuileMain < 6; indiceTuileMain++)
            {
                if(main[indiceTuileMain].GetCouleur() == "jaune")
                    occurenceCouleur[0]++;
                if (main[indiceTuileMain].GetCouleur() == "rouge")
                    occurenceCouleur[1]++;
                if (main[indiceTuileMain].GetCouleur() == "vert")
                    occurenceCouleur[2]++;
                if (main[indiceTuileMain].GetCouleur() == "orange")
                    occurenceCouleur[3]++;
                if (main[indiceTuileMain].GetCouleur() == "violet")
                    occurenceCouleur[4]++;
                if (main[indiceTuileMain].GetCouleur() == "bleu")
                    occurenceCouleur[5]++;
            }
            for (int indiceTuileMain = 0; indiceTuileMain < 6; indiceTuileMain++)
            {
                if (main[indiceTuileMain].GetSymbole() == "cercle")
                    occurenceSymbole[0]++;
                if (main[indiceTuileMain].GetSymbole() == "etoile")
                    occurenceSymbole[1]++;
                if (main[indiceTuileMain].GetSymbole() == "trefle")
                    occurenceSymbole[2]++;
                if (main[indiceTuileMain].GetSymbole() == "carre")
                    occurenceSymbole[3]++;
                if (main[indiceTuileMain].GetSymbole() == "losange")
                    occurenceSymbole[4]++;
                if (main[indiceTuileMain].GetSymbole() == "croix")
                    occurenceSymbole[5]++;
            }

            if (occurenceCouleur.Max() > occurenceSymbole.Max())
                return occurenceCouleur.Max();
            else
                return occurenceSymbole.Max();            
        }
    }
}
