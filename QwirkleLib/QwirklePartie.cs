﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using QwirkleLib;

namespace QwirkleLib
{
    public static class QwirklePartie
    {
        //Attributs
        private static Joueur[] joueurs = new Joueur[4];
        private static Pioche pioche = new Pioche();
        private static Grille plateauDeJeu = new Grille();
        private static int nombreJoueur;
        private static int joueurActuel;

        static QwirklePartie()
        {
            for (int indiceTableauJoueur = 0; indiceTableauJoueur < 4; indiceTableauJoueur++)
            {
                joueurs[indiceTableauJoueur] = new Joueur();
            }
            joueurActuel = 0;
        }

        //Methodes
        //Accesseurs
        static public void SetNombreJoueur(int nombreJoueurSaisie)
        {
            nombreJoueur = nombreJoueurSaisie;
        }

        static public int GetNombreJoueur()
        {
            return nombreJoueur;
        }

        static public void SetNomJoueur(string nom, int numeroJoueur)
        {
            joueurs[numeroJoueur - 1].SetNom(nom);
        }

        static public string GetNomJoueur(int numeroJoueur)
        {
            return joueurs[numeroJoueur - 1].GetNom();
        }

        static public string GetNomJoueurActuel()
        {
            return joueurs[joueurActuel].GetNom();
        }

        static public int GetNombreTuileRestantePioche()
        {
            return pioche.GetNombreDeTuile();
        }

        static public string GetCouleurTuileMainJoueur(int indiceTuileMain)
        {
            return joueurs[joueurActuel].GetTuileMain(indiceTuileMain - 1).GetCouleur();
        }

        static public string GetSymboleTuileMainJoueur(int indiceTuileMain)
        {
            return joueurs[joueurActuel].GetTuileMain(indiceTuileMain - 1).GetSymbole();
        }

        static public int GetTailleGrille()
        {
            return plateauDeJeu.GetTaille();
        }

        static public bool GetEtatTuileGrille(int x, int y)
        {

            return plateauDeJeu.GetTuile(x, y).GetEtat();
        }

        static public int GetNombreTuilePose()
        {
            return plateauDeJeu.GetNombreTuilePose();
        }

        static public int GetIndiceJoueurActuel()
        {
            return joueurActuel + 1;
        }

        static public int GetAncienneTailleGrille()
        {
            return plateauDeJeu.GetAncienneTaille();
        }

        static public string GetCouleurTuileGrille(int x, int y)
        {
            return plateauDeJeu.GetCouleurTuile(x, y);
        }

        static public string GetSymboleTuileGrille(int x, int y)
        {
            return plateauDeJeu.GetSymboleTuile(x, y);
        }

        //Methodes appelés au début de partie
        static public void DistributionTuile()
        {
            for (int indiceTableauJoueur = 0; indiceTableauJoueur < nombreJoueur; indiceTableauJoueur++)
            {
                for (int indiceTuileMain = 0; indiceTuileMain < 6; indiceTuileMain++)
                {
                    joueurs[indiceTableauJoueur].SetTuileMain(pioche.GetTuileAleatoire(), indiceTuileMain);
                }
            }

        }

        //Renvoi l'indice du premier joueur
        static public void RecherchePremierJoueur()
        {
            int[] valeurMaxCaractCommun = new int[] { 0, 0, 0, 0 };
            for (int indiceTableauJoueur = 0; indiceTableauJoueur < nombreJoueur; indiceTableauJoueur++)
            {
                valeurMaxCaractCommun[indiceTableauJoueur] = joueurs[indiceTableauJoueur].GetNombreCaractCommune();

            }
            joueurActuel = Array.IndexOf(valeurMaxCaractCommun, valeurMaxCaractCommun.Max());
        }

        //MEthode de selection des tuiles de la main
        static public void SelectionTuileMain(int indiceTuileMain)
        {
            joueurs[joueurActuel].SelectionTuile(indiceTuileMain - 1);
        }

        static public void DeselectionTuileMain(int indiceTuileMain)
        {
            joueurs[joueurActuel].DeselectionTuile(indiceTuileMain - 1);
        }

        static public void ResetSelectionTuileMain()
        {
            joueurs[joueurActuel].ResetSelectionTuile();
        }

        //Methodes pour l'echange de tuiles
        static public bool VerificationSelectionTuileMain(int indiceTuileMain)
        {
            if (joueurs[joueurActuel].VerificationSelectionTuile(indiceTuileMain - 1))
                return true;
            else
                return false;
        }

        static public bool VerificationSelectionActive()
        {
            for (int indiceTuileMain = 0; indiceTuileMain < 6; indiceTuileMain++)
            {
                if (joueurs[joueurActuel].VerificationSelectionTuile(indiceTuileMain) == true)
                    return true;
            }
            return false;
        }

        static public void EchangeTuile()
        {
            for (int indiceTuileMain = 0; indiceTuileMain < 6; indiceTuileMain++)
            {
                if (joueurs[joueurActuel].VerificationSelectionTuile(indiceTuileMain) == true)
                {
                    pioche.RemiseTuile(joueurs[joueurActuel].GetTuileMain(indiceTuileMain));
                    joueurs[joueurActuel].SetTuileMain(pioche.GetTuileAleatoire(), indiceTuileMain);
                }

            }
        }

        //Methodes pour placer un mot
        static public bool AutorisationPlacementTuile(int indiceTuileMain, int x, int y)
        {
            return plateauDeJeu.VerificationLegalitePlacement(joueurs[joueurActuel].GetTuileMain(indiceTuileMain), x, y);
        }

        static public void PlacementTuile(int indiceTuileMain, int x, int y)
        {
            plateauDeJeu.SetTuile(joueurs[joueurActuel].GetTuileMain(indiceTuileMain), x, y);
            joueurs[joueurActuel].GetTuileMain(indiceTuileMain).SetEtat(true);
        }

        //Methode bouton reinitialiser le tour
        static public void ResetGrilleTourDuJoueur()
        {
            plateauDeJeu.GrilleResetTour();
        }

        static public void ResetPremierCoup()
        {
            plateauDeJeu.ResetPremierCoup();
        }

        static public void ReinitialisationGrille()
        {
            plateauDeJeu.GrilleReinitialisationTour();
        }

        //Methode Fin de tour
        static public void JoueurSuivant()
        {
            joueurActuel++;
            if (joueurActuel == nombreJoueur)
                joueurActuel = 0;
        }

        static public bool VerificationAucuneTuilePlacee()
        {
            if (plateauDeJeu.GetNombreTuilePose() > 0)
                return false;
            else
                return true;
        }

        static public int CalculDuScore()
        {
            joueurs[joueurActuel].AjouterScore(plateauDeJeu.CalculScore());
            return joueurs[joueurActuel].GetScore();
        }

        static public bool VerificationFinDePartie()
        {
            bool retourVerificationFinDePartie = true;
            //Le joueur a joué toutes ses tuiles
            for (int indiceTuileMain = 0; indiceTuileMain < 6; indiceTuileMain++)
            {
                if (joueurs[joueurActuel].GetTuileMain(indiceTuileMain).GetCouleur() != "defaut")
                {
                    return false;
                }

            }

            if (retourVerificationFinDePartie)
                return true;

            //Aucun joueur ne peut plus rien joueur
            for (int indiceJoueur = 0; indiceJoueur < nombreJoueur; indiceJoueur++)//Boucle Joueur
            {
                for (int indiceTuileMain = 0; indiceTuileMain < 6; indiceTuileMain++)
                {
                    for (int ligne = 0; ligne < plateauDeJeu.GetTaille(); ligne++)
                    {
                        for (int colonne = 0; colonne < plateauDeJeu.GetTaille(); colonne++)
                        {
                            if (pioche.GetNombreDeTuile() == 0 && plateauDeJeu.VerificationLegalitePlacement(joueurs[joueurActuel].GetTuileMain(indiceTuileMain), ligne, colonne))
                                return false;
                        }
                    }
                }
            }

            return retourVerificationFinDePartie;
        }

        //methode qui permet de piocher a la fin d'un tour
        static public void PiocheTuile()
        {
            for (int indiceTuile = 0; indiceTuile < 6; indiceTuile++)
            {
                if (joueurs[joueurActuel].GetTuileMain(indiceTuile).GetEtat())
                    joueurs[joueurActuel].SetTuileMain(pioche.GetTuileAleatoire(), indiceTuile);
            }
        }

        //Gestion dynamique de la grille

        static public bool ActualisationTailleGrille()
        {
            return plateauDeJeu.ActualisationGrille();
        }

        //Fin de partie
        static public string GetClassementJoueur(int indiceJoueur)
        {
            string retourClassement, position_as_str = "";
            int position = 1;
            if (indiceJoueur > nombreJoueur)
            {
                return " ";
            }

            for (int indiceTableauJoueur = 0; indiceTableauJoueur < 4; indiceTableauJoueur++)
            {
                if (indiceJoueur != indiceTableauJoueur + 1 && joueurs[indiceTableauJoueur].GetScore() > joueurs[indiceJoueur - 1].GetScore())
                    position++;
            }

            switch (position)
            {
                case 1:
                    position_as_str = "1er";
                    break;
                case 2:
                    position_as_str = "2eme";
                    break;
                case 3:
                    position_as_str = "3eme";
                    break;
                case 4:
                    position_as_str = "4eme"; ;
                    break;

            }
            retourClassement = String.Format("{0} : {1}", position_as_str, joueurs[indiceJoueur - 1].GetNom());
            return retourClassement;
        }

        static public void Reinitialisation()
        {
            for (int indiceTableauJoueur = 0; indiceTableauJoueur < 4; indiceTableauJoueur++)
            {
                joueurs[indiceTableauJoueur] = new Joueur();
            }
            joueurActuel = 0;
            pioche = new Pioche();
            plateauDeJeu = new Grille();
        }

    }



}
